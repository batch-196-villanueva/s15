console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	let firstNameLabel = "First Name:";
	let firstName = "Max";
	console.log(firstNameLabel, firstName);

	let lastNameLabel = "Last Name:";
	let lastName = "Mayfield";
	console.log(lastNameLabel, lastName);

	let ageLabel = "Age:";
	let age = 15;
	console.log(ageLabel, age);

	let hobbiesLabel = "Hobbies:";
	const hobbies = ["Skating", "Listening to Kate Bush", "Getting Vecna'd"]
	console.log(hobbiesLabel, hobbies);

	let addressLabel = "Address: ";
	const address = {
		houseNumber: "767",
		street: "Somewhere",
		city: "Hawkins",
		state: "Indiana"
	};
	console.log(addressLabel, address);


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let name = "Steve Rogers";
	console.log("My full name is " + name);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	const friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
